import React from "react";

function Home() {
  return (
    <section className="home_section">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>Home Page</h3>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Home;
