import React from "react";
import { NavLink, useLocation } from "react-router-dom";
import { useAuth } from "../Utils/auth";

function Navbar() {
  //useLocation hooks returns loaction object provided by react-router
  const location = useLocation();
  const auth = useAuth();
  return (
    <section className="nav_section">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <nav className="navbar navbar-default">
              <div className="container-fluid">
                <div className="navbar-header">
                  <button
                    type="button"
                    className="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1"
                    aria-expanded="false"
                  >
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                  <NavLink to="/" className="navbar-brand">
                    Dashboard
                  </NavLink>
                </div>
                <div
                  className="collapse navbar-collapse"
                  id="bs-example-navbar-collapse-1"
                >
                  <ul className="nav navbar-nav">
                    <li>
                      <NavLink to="home">Home</NavLink>
                    </li>
                  </ul>
                  <ul className="nav navbar-right">
                    {location.pathname !== "/signin" && !auth.user ? (
                      <li>
                        <NavLink to="signin">
                          <button
                            type="button"
                            className="btn btn-default signin_btn"
                          >
                            Sign IN
                          </button>
                        </NavLink>
                      </li>
                    ) : null}
                  </ul>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Navbar;
