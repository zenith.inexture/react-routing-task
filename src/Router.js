import React from "react";
import { Route, Routes } from "react-router-dom";
import Dashboard from "./ProtectedScreen/Dashboard";
import Home from "./Components/Home";
import { RequireAuth } from "./Utils/RequireAuth";
import Signin from "./Screen/Signin";
import Signup from "./Screen/Signup";

function Router() {
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <RequireAuth>
              <Dashboard />
            </RequireAuth>
          }
        ></Route>
        <Route path="home" element={<Home />}></Route>
        <Route path="signin" element={<Signin />}></Route>
        <Route path="signup" element={<Signup />}></Route>
      </Routes>
    </>
  );
}

export default Router;
