import * as Yup from "yup";

//assign initial values
const initialValues = {
  username: "",
  email: "",
  password: "",
  cpassword: "",
  phone: "",
  state: "select",
  photo: "",
};
// const SUPPORTED_FORMATS = ["image/jpg", "image/jpeg", "image/png"];
//validation with yup object
const validationSchema = Yup.object({
  username: Yup.string().required("Required"),
  email: Yup.string()
    .matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, "Invalid email format")
    .required("Required"),
  password: Yup.string()
    .min(8, "Password is too short - should be 8 chars minimum.")
    .matches(/^[a-z0-9]+$/i, "Password can only contain letters and numbers")
    .required("Required"),
  cpassword: Yup.string()
    .oneOf([Yup.ref("password")], "Confirm password must match")
    .required("Required"),
  phone: Yup.string()
    .matches(
      /^[0-9]{10}$/,
      "phone can only contain numbers and exact 10 numbers are allowed"
    )
    .required("Required"),
  state: Yup.string()
    // .required("Required"),
    .test("", "Required", (values) => {
      if (values === "select") {
        return false;
      } else {
        return true;
      }
    }),
  photo: Yup.string()
    .required("Required")
    .test("", "Only jpg, jpeg and png file are accepted", (value) => {
      if (value) {
        const extension = value.split(".")[1];
        if (
          extension === "jpg" ||
          extension === "jpeg" ||
          extension === "png"
        ) {
          console.log("thai");
          return true;
        } else {
          console.log("na thai");
          return false;
        }
      }
    }),
});

export { initialValues, validationSchema };
