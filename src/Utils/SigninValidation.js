import * as Yup from "yup";

//assign initial values
const initialValues = {
  signin_username: "",
  signin_password: "",
};

//validation with yup object
const validationSchema = Yup.object({
  signin_username: Yup.string()
    .required("Required")
    .test("", "data doesn't exist", (values) => {
      const retrievedObject = JSON.parse(localStorage.getItem("values"));
      console.log(values);
      if (values === retrievedObject.username) {
        return true;
      } else {
        return false;
      }
    }),
  signin_password: Yup.string()
    .required("Required")
    .test("", "data doesn't exist", (values) => {
      const retrievedObject = JSON.parse(localStorage.getItem("values"));
      console.log(values);
      if (values === retrievedObject.password) {
        return true;
      } else {
        return false;
      }
    }),
});

export { initialValues, validationSchema };

//we can do the individual validation for the username and password but this is not the currect way
//it's easy for hacker to hack any website with this loop hole
