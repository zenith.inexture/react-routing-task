import React from "react";
import { NavLink } from "react-router-dom";
import { useAuth } from "../Utils/auth";

function Dashboard() {
  const auth = useAuth();
  const handlesignout = () => {
    auth.signout();
  };
  return (
    <section className="dashboard_section">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h3>Welcome {auth.user}</h3>
            <NavLink to="/">
              <button className="btn btn-success" onClick={handlesignout}>
                Sign Out
              </button>
            </NavLink>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Dashboard;
