import "./Assets/CSS/App.css";
import { AuthProvider } from "./Utils/auth";
import Navbar from "./Components/Navbar";
import Router from "./Router";

function App() {
  return (
    <>
      <AuthProvider>
        <Navbar />
        <Router />
      </AuthProvider>
    </>
  );
}

export default App;
